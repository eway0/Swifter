﻿using Swifter.Json;
using Swifter.MessagePack;
using Swifter.Reflection;
using Swifter.RW;
using Swifter.Tools;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;

namespace Swifter.Test.Debug
{
    public sealed class Demo
    {
        public static unsafe void Main()
        {
            var data = "qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm~!@#$%^&*()_+\"'\\//";

            data = data + data + data + data + data;

            var json = JsonFormatter.SerializeObject(data);

            Console.WriteLine(json.Length);

            Console.WriteLine(json);

            //for (int i = 0; i < 10000000; i++)
            //{
            //    JsonFormatter.SerializeObject(data);
            //}
        }
    }
}