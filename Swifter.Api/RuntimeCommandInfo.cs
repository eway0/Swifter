﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Swifter.Api
{
    public sealed class RuntimeCommandInfo
    {
        internal RuntimeCommandInfo(string name, Version version, ICommandProcessor processor)
        {
            Name = name;
            Version = version;
            Processor = processor;

            parameters = new Dictionary<string, ParameterInfo>();
            results = new Dictionary<string, ResultInfo>();
        }

        /// <summary>
        /// 指示命令需要授权。
        /// </summary>
        public bool NeedAuthorized;

        /// <summary>
        /// 命令名称。
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// 命令版本。
        /// </summary>
        public readonly Version Version;

        /// <summary>
        /// 命令的缓存键。
        /// </summary>
        public string CacheKey;

        /// <summary>
        /// 命令的锁键。
        /// </summary>
        public string LockKey;

        /// <summary>
        /// 命令的前置触发器。
        /// </summary>
        public string[] BeforeTriggers;

        /// <summary>
        /// 命令的后置触发器。
        /// </summary>
        public string[] AfterTriggers;

        /// <summary>
        /// 命令的执行器。
        /// </summary>
        public ICommandInvoker Invoker;

        /// <summary>
        /// 命令处理器。
        /// </summary>
        public readonly ICommandProcessor Processor;

        /// <summary>
        /// 参数集合。
        /// </summary>
        internal readonly Dictionary<string, ParameterInfo> parameters;

        /// <summary>
        /// 返回值集合。
        /// </summary>
        internal readonly Dictionary<string, ResultInfo> results;

        public sealed class ParameterInfo
        {
            public string Name { get; set; }

            public Type Type { get; set; }

            public object DefaultValue { get; set; }
        }

        public sealed class ResultInfo
        {
            public string Name { get; set; }

            public Type Type { get; set; }
        }
    }
}
