﻿namespace Swifter.Api.Types
{
    public interface ILengthedType : ITypeInfo
    {
        int Length { get; }
    }
}
