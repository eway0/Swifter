﻿using Swifter.RW;
using System;

namespace Swifter.Api.Types
{
    public abstract class BaseLengthedText<T> : ILengthedType where T : BaseLengthedText<T>, new()
    {
        public string Content;

        public abstract int Length { get; }

        public sealed class Interface : IValueInterface<T>
        {
            public T ReadValue(IValueReader valueReader)
            {
                var content = ValueInterface<string>.ReadValue(valueReader);

                if (content == null)
                {
                    throw new NullReferenceException($"Type: '{typeof(T)}' cannot be null.");
                }

                var result = new T { Content = content };

                if (content.Length > result.Length)
                {
                    throw new FormatException($"Type: '{typeof(T)}' length out of limit.");
                }

                return result;
            }

            public void WriteValue(IValueWriter valueWriter, T value)
            {
                ValueInterface<string>.WriteValue(valueWriter, value.Content);
            }
        }
    }

    public sealed class Text100 : BaseLengthedText<Text100>
    {
        public override int Length => 100;
    }

    public sealed class Text256 : BaseLengthedText<Text256>
    {
        public override int Length => 256;
    }

    public sealed class Text512 : BaseLengthedText<Text512>
    {
        public override int Length => 512;
    }

    public sealed class Text1024 : BaseLengthedText<Text1024>
    {
        public override int Length => 1024;
    }

    public sealed class LongText : BaseLengthedText<LongText>
    {
        public override int Length => int.MaxValue;
    }
}
