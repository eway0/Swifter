﻿using Swifter.RW;

namespace Swifter.Api.Types
{
    sealed class ApplicationParameter : ITypeInfo, IValidator, IValueInterface<ApplicationParameter>
    {
        public object value;

        public ApplicationParameter ReadValue(IValueReader valueReader)
        {
            return new ApplicationParameter { value = valueReader.DirectRead() };
        }

        public void Verify(ContextInfo contextInfo, ContextParameters parameters, string name)
        {
            value = contextInfo.Application.GetApplicationParameter(name, contextInfo, parameters).Value;
        }

        public void WriteValue(IValueWriter valueWriter, ApplicationParameter value)
        {
            ValueInterface.WriteValue(valueWriter, value.value);
        }
    }
}