﻿namespace Swifter.Api.Types
{
    /// <summary>
    /// 表示一个值验证器。
    /// </summary>
    public interface IValidator
    {
        /// <summary>
        /// 验证该值是否允许出现在这个上下文中，如果不允许则抛出异常。
        /// </summary>
        /// <param name="contextInfo">上下文</param>
        /// <param name="parameters">上下文参数</param>
        /// <param name="name">参数名称</param>
        void Verify(ContextInfo contextInfo, ContextParameters parameters, string name);
    }
}
