﻿using System;
using System.Collections.Generic;

namespace Swifter.Api
{
    public sealed class ApplicationState
    {
        /// <summary>
        /// 服务器名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 服务器 Id。
        /// </summary>
        public Guid ApplicationId { get; set; }

        /// <summary>
        /// 服务器各个协议的 Url。
        /// </summary>
        public Dictionary<string, string> ApplicationUrls { get; set; }

        /// <summary>
        /// 服务器可以容纳的最大上下文数量。
        /// </summary>
        public int MaxCapacity { get; set; }

        /// <summary>
        /// 服务器当前大上下文数量。
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 服务器总内存量（Bytes）。
        /// </summary>
        public long TotalMemory { get; set; }

        /// <summary>
        /// 服务器已使用内存量（Bytes）。
        /// </summary>
        public long UsedMemory { get; set; }

        /// <summary>
        /// 服务器总内硬盘容量（Bytes）。
        /// </summary>
        public long TotalDick { get; set; }

        /// <summary>
        /// 服务器已使用硬盘容量（Bytes）。
        /// </summary>
        public long UsedDick { get; set; }

        /// <summary>
        /// 服务器总频率（Hz）。
        /// </summary>
        public long TotalFrequency { get; set; }

        /// <summary>
        /// 服务器在一分钟内使用频率的平均值（Hz）。
        /// </summary>
        public long UsedFrequency { get; set; }
    }
}