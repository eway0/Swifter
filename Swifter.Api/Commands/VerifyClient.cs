﻿using System;
using System.Collections.Generic;

namespace Swifter.Api.Commands
{
    public class VerifyClient : BaseNativeSyncCommandInfo<VerifyClient>
    {
        public Guid ClientId { get; set; }

        public Guid DeviceId { get; set; }

        public Dictionary<string, string> DeviceParameters { get; set; }

        public override Version Version => Version.Parse("0.0.0");

        public override object Invoke(ContextInfo contextInfo, ContextParameters parameters)
        {
            if (contextInfo.client_id != ClientId)
            {
                throw new ParameterException(nameof(ClientId), ParameterExceptionCode.Incorrect);
            }

            if (contextInfo.device_id != DeviceId)
            {
                throw new ParameterException(nameof(DeviceId), ParameterExceptionCode.Incorrect);
            }

            return "ok";
        }
    }
}