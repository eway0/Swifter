﻿namespace Swifter.Api
{
    /// <summary>
    /// 命令标识符
    /// </summary>
    public enum CommandFlags : int
    {
        /// <summary>
        /// 表示这个命令是执行查询操作的。
        /// </summary>
        Select = 0x1,
        /// <summary>
        /// 表示这个命令是执行新增操作的。
        /// </summary>
        Insert = 0x2,
        /// <summary>
        /// 表示这个命令是执行修改操作的。
        /// </summary>
        Update = 0x4,
        /// <summary>
        /// 表示这个命令是执行删除操作的。
        /// </summary>
        Delete = 0x8,
        /// <summary>
        /// 表示这个命令是一个触发器。
        /// </summary>
        Trigger = 0x8,
        /// <summary>
        /// 表示这个命令需要授权才能调用。
        /// </summary>
        Authorization = 0x20,
        /// <summary>
        /// 表示这个命令只能内部调用。
        /// </summary>
        InternalCall = 0x40,
        /// <summary>
        /// 使用内存缓存。
        /// </summary>
        MemoryCache = 0x80,
        /// <summary>
        /// 使用磁盘缓存。
        /// </summary>
        DiskCache = 0x100,
    }
}
