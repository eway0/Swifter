﻿using Swifter.RW;
using System;

#nullable enable

namespace Swifter.Api
{
    public abstract class BaseNativeSyncCommandInfo<T> : INativeCommandInfo where T : BaseNativeSyncCommandInfo<T>
    {
        public string Name => typeof(T).Name;

        public abstract Version Version { get; }

        public abstract object? Invoke(ContextInfo contextInfo, ContextParameters parameters);

        public void Invoke(ContextInfo context, ContextParameters parameters, Action<object?> callback)
        {
            callback(ValueInterface<T>.ReadValue(ValueCopyer.ValueOf(parameters.CommandParameters)).Invoke(context, parameters));
        }
    }
}