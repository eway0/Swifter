﻿using System;

namespace Swifter.Api
{
    public sealed class ContextResults
    {
        /// <summary>
        /// 结果是否来自缓存。
        /// </summary>
        public DateTimeOffset? CacheTime { get; set; }

        /// <summary>
        /// 处理所用时间。
        /// </summary>
        public long UsedTime { get; set; }

        /// <summary>
        /// 返回值代码。
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 返回值消息。
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 异常类型。
        /// </summary>
        public string ExceptionType { get; set; }

        /// <summary>
        /// 服务器时间。
        /// </summary>
        public DateTimeOffset ServerTime { get; set; }

        /// <summary>
        /// 结果对象。
        /// </summary>
        public object Data { get; set; }
    }
}
