﻿namespace Swifter.Data.Sql
{
    /// <summary>
    /// 表示一个常量值。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ConstantValue<T> : SqlValue<T>
    {
        /// <summary>
        /// 值
        /// </summary>
        public override T Value { get; }

        internal ConstantValue(T value)
        {
            Value = value;
        }
    }
}