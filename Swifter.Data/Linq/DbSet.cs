﻿using Swifter.Data;
using Swifter.Data.Sql;
using Swifter.RW;

namespace Swifter.Data.Linq
{
    /// <summary>
    /// 表示一个数据查询集。
    /// </summary>
    /// <typeparam name="TSource">实体类型</typeparam>
    public sealed class DbSet<TSource> : DbQuery<TSource>
    {
        /// <summary>
        /// 初始化具有默认名称的数据查询集。
        /// </summary>
        /// <param name="database">数据库实例</param>
        public DbSet(Database database) : this(database, typeof(TSource).Name)
        {

        }

        /// <summary>
        /// 初始化指定表格的数据查询集。
        /// </summary>
        /// <param name="database">数据库实例</param>
        /// <param name="table">指定表格</param>
        public DbSet(Database database, Table table) : base(database, new SelectStatement(table))
        {
            SetColumns();
        }

        private void SetColumns()
        {
            var writer = RWHelper.CreateWriter<TSource>().As<string>();

            foreach (var key in writer.Keys)
            {
                SelectStatement.MainColumn(key);
            }
        }
    }
}